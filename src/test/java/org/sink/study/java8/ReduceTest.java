package org.sink.study.java8;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Stream;

public class ReduceTest {


    @Test
    public void testSum() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);

        // 没有起始值时返回为Optional类型
        Optional<Integer> sumOptional = integers.stream().reduce(Integer::sum);
        System.out.println(sumOptional.get());

        // 可以给一个起始种子值
        Integer sumReduce = integers.stream().reduce(0, Integer::sum);
        System.out.println(sumReduce);

        //直接用sum方法
        Integer sum = integers.stream().mapToInt(i -> i).sum();
        System.out.println(sum);
    }

    @Test
    public void testConcat() {
        //构造字符串流
        List<String> strs = Arrays.asList("H", "E", "L", "L", "O");

        // reduce
        String concatReduce = strs.stream().reduce("", String::concat);
        System.out.println(concatReduce);
    }

    @Test
    public void testMin() {

        //min reduce
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        Integer minReduce = integerStream.reduce(Integer.MAX_VALUE, Integer::min);
        System.out.println(minReduce);


        // min
        Stream<Integer> integerStream1 = Stream.of(1, 2, 3, 4, 5);
        OptionalInt min = integerStream1.mapToInt(i -> i).min();
        System.out.println(min.getAsInt());
    }

    @Test
    public void testMax() {

        //max reduce
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        Integer maxReduce = integerStream.reduce(Integer.MIN_VALUE, Integer::max);
        System.out.println(maxReduce);


        // max
        Stream<Integer> integerStream1 = Stream.of(1, 2, 3, 4, 5);
        OptionalInt max = integerStream1.mapToInt(i -> i).max();
        System.out.println(max.getAsInt());
    }


}
