package org.sink.study.java8;

import org.junit.Test;

import java.util.Optional;
import java.util.function.Supplier;

public class OptionalTest {

    @Test
    public void testOf() {
        Optional<String> helloWorld = Optional.of("Hello World");
        Optional<String> nullOptional = Optional.of(null);
    }

    @Test
    public void testOfNullable() {
        Optional<String> nullOptional = Optional.ofNullable(null);
    }

    @Test
    public void testIsPresent() {
        Optional<String> hello = Optional.ofNullable("Hello World");
        System.out.println(hello.isPresent());
        Optional<String> nullHello = Optional.ofNullable(null);
        System.out.println(nullHello.isPresent());
    }

    @Test
    public void testIfPresent() {
        Optional<String> hello = Optional.ofNullable("Hello World");
        hello.ifPresent(value -> System.out.println("present!"));
        Optional<String> nullHello = Optional.ofNullable(null);
        nullHello.ifPresent(value -> System.out.println("present too?"));
    }

    @Test
    public void testOrElse() {
        Optional<String> hello = Optional.ofNullable("Hello World");
        String str1 = hello.orElse("I will not be show");
        System.out.println(str1);
        Optional<String> nullHello = Optional.ofNullable(null);
        String str2 = nullHello.orElse("I am null");
        System.out.println(str2);
    }

    @Test
    public void testOrElseGet() {
        Supplier<String> strSupplier = () -> "Default Str";

        Optional<String> hello = Optional.ofNullable("Hello World");
        String str1 = hello.orElseGet(strSupplier);
        System.out.println(str1);

        Optional<String> nullHello = Optional.ofNullable(null);
        String str2 = nullHello.orElseGet(strSupplier);
        System.out.println(str2);
    }

    @Test
    public void testOrElseThrow() {
        try {
            Optional<String> hello = Optional.ofNullable("Hello World");
            hello.orElseThrow(Exception::new);

            Optional<String> nullHello = Optional.ofNullable(null);
            nullHello.orElseThrow(Exception::new);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMap() {
        Optional<String> hello = Optional.ofNullable("Hello World");
        Optional<String> nullHello = Optional.ofNullable(null);
        Optional<Integer> integer = hello.map(value -> 1);
        Optional<Integer> integer1 = nullHello.map(value -> 1);
        System.out.println(integer.isPresent());
        System.out.println(integer1.isPresent());
    }

    @Test
    public void testFlatMap() {
        Optional<String> hello = Optional.ofNullable("Hello World");
        hello.flatMap(value -> Optional.of(value + " boy"));
        String str1 = hello.orElse("no value");
        System.out.println(str1);

        Optional<String> nullHello = Optional.ofNullable(null);
        nullHello.flatMap(value -> Optional.of(value + " boy"));
        String str2 = nullHello.orElse("no value");
        System.out.println(str2);
    }

    @Test
    public void testFilter() {
        Optional<Integer> integer = Optional.of(4);
        integer.filter(v -> v > 3).ifPresent(v -> System.out.println(v + " > 3"));
        System.out.println(integer.orElse(0));;
    }

}
