package org.sink.study.gradle;

import java.util.function.Function;

public class FunctionTest {
    public static void main(String[] args) {
        Function<Integer, String> intToStrFunc = intVal -> String.valueOf(intVal);
        Function<String, String> strToStrFunc = intToStrFunc.compose(strVal -> Integer.valueOf(strVal));
        strToStrFunc.apply("123");
    }
}
