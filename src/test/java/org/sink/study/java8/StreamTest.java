package org.sink.study.java8;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest {

    @Test
    public void testFilter() {
        List<Integer> intList = new ArrayList<>();
        intList.add(14);
        intList.add(5);
        intList.add(43);
        intList.add(89);
        intList.add(64);
        intList.add(112);
        intList.add(55);
        intList.add(55);
        intList.add(58);

        intList.stream().filter(intNum -> intNum != null)
                .filter(intNum -> intNum > 50)
                .forEach(System.out::println);
        System.out.println(intList);
        List<Integer> sortedIntList = intList.parallelStream().sorted().collect(Collectors.toList());
        System.out.println(sortedIntList);
        Stream<Integer> integerStream = intList.parallelStream();
        Stream<Integer> sorted = integerStream.sorted();
        List<Integer> collect = sorted.collect(Collectors.toList());
        System.out.println(collect);
    }

    @Test
    public void testGroupBy() {
        List<Integer> integers = Arrays.asList(2, 3, 8, 4, 4, 2, 0, 4, 0);
        Map<Integer, List<Integer>> intMap = integers.parallelStream().collect(Collectors.groupingBy((i) -> i));
        System.out.println(intMap);
    }

    @Test
    public void testMapToInt() {
        List<Integer> integers = Arrays.asList(2, 3, 8, 4, 4, 2, 0, 4, 0);
        int[] ints = integers.stream().mapToInt(i -> 2 * i).toArray();
        System.out.println(Arrays.toString(ints));

        // method 1
        List<Integer> collect = integers.stream().mapToInt(i -> 2 * i).boxed().collect(Collectors.toList());
        System.out.println(collect);

        // method 2
        List<Integer> collect1 = integers.stream().mapToInt(i -> 2 * i).mapToObj(i -> i).collect(Collectors.toList());
        System.out.println(collect1);


    }

    @Test
    public void testFlatMap() {

        Map<Integer, List<Integer>> intMaps = new HashMap<>();
        intMaps.put(1, Arrays.asList(1, 2, 3, 4, 5));
        intMaps.put(2, Arrays.asList(2, 3, 4, 5, 6));
        intMaps.put(3, Arrays.asList(3, 4, 5, 6, 7));
        intMaps.put(4, Arrays.asList(4, 5, 6, 7, 8));
        intMaps.put(5, Arrays.asList(5, 6, 7, 8, 9));

        /* 把intMaps中的所有values放在一个集合中并去重 */

        Set<Map.Entry<Integer, List<Integer>>> entries = intMaps.entrySet();

        // java8之前的做法
        Set<Integer> result = new HashSet<>();
        for (Map.Entry<Integer, List<Integer>> entry : entries) {
            result.addAll(entry.getValue());
        }
        System.out.println(result);

        // java8的做法
        Set<Integer> result8 = entries.stream()
                .flatMap(entry -> entry.getValue().stream())
                .collect(Collectors.toSet());
        System.out.println(result8);

    }
}
