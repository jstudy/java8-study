package org.sink.study.gradle;

import java.util.Arrays;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World");
        Arrays.asList("a", "b", "c").forEach(System.out::println);
    }
}
